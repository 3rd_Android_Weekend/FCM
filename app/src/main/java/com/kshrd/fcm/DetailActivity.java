package com.kshrd.fcm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String id = getIntent().getExtras().getString("id");
        String result = getIntent().getExtras().getString("result");

        TextView tvResult = (TextView) findViewById(R.id.tvResult);
        tvResult.setText("id -> " + Integer.parseInt(id) + " , result -> " + result);

    }
}
