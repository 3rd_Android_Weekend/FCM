package com.kshrd.fcm;

import android.app.Application;

import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by pirang on 8/5/17.
 */

public class FCMApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseMessaging.getInstance().subscribeToTopic("news");
    }

}
