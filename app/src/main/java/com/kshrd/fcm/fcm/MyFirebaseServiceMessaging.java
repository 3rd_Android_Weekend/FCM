package com.kshrd.fcm.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kshrd.fcm.DetailActivity;
import com.kshrd.fcm.R;

/**
 * Created by pirang on 7/30/17.
 */

public class MyFirebaseServiceMessaging extends FirebaseMessagingService {

    private static final String TAG = "ooooo";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String title = remoteMessage.getNotification().getTitle();
        String content = remoteMessage.getNotification().getBody();

        String id = null, result = null;
        if (remoteMessage.getData().size() > 0){
            id = remoteMessage.getData().get("id");
            result = remoteMessage.getData().get("result");
            Log.e(TAG, "id -> " + id + " , result -> " + result);
        }

        Intent i = new Intent(this, DetailActivity.class);
        i.putExtra("id", id);
        i.putExtra("result", result);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(notificationId, builder.build());

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

}
